import numpy as np
from .. import Layers,Losses,Optimizers

class BaseNetwork(object):

	def __init__(self,list_of_layers,loss_name,optimizer_name,input_sample,learningrate,norm=0,task='classification'):
		"""Summary
		
		Args:
		    list_of_layers (list): list of Layer objects
		    loss_name (str): name of loss fxn to use
		    optimizer_name (str): name of optimizer to implement
		    input_sample (np.array): single input observation
		"""

		self.layers = list_of_layers
		self.loss = Losses.get(loss_name)
		self.task = task
		self.optimizer = Optimizers.get(optimizer_name)

		for layer in self.layers:
			layer.loss = self.loss
			layer.norm = norm
			layer.learningrate=learningrate



	def feed_forward(self,data_input):
		# Return list of layer activations
		activations = [data_input]
		for layer in self.layers:
			activation = layer.activate(activations[len(activations)-1])
			activations.append(activation)

		return activations

	def predict(self,data_input):
		# Just return the output layer. Less memory intense
		predictions = data_input
		for layer in self.layers:
			predictions = layer.activate(predictions)

		return predictions

	def backprop_gd(self,pred,actual,activations):
		# Expects actual to be one-hot encoded if task="classify"
		activ = self.layers[len(self.layers)-1].activation
		dl=self.loss['deriv'](pred,actual)*activ['deriv'](pred)
		activations.reverse()
		previous_activations = activations[1:]
		
		for this_layer,previous_activation in zip(self.layers[::-1],previous_activations):
			dCdW=GD.dC(dl,previous_activation)

			if isinstance(this_layer.biases,np.ndarray):
				this_layer.biases = this_layer.biases-this_layer.learningrate*dl.mean(axis=0)

			dl = np.matmul(dl,this_layer.weights.transpose())*this_layer.activation['deriv'](previous_activation)
			
			if this_layer.norm:
				this_layer.weights = (1-this_layer.norm)*this_layer.weights

			this_layer.weights = this_layer.weights - this_layer.learningrate*dCdW

	def get_loss(self,pred,actual):
		return self.loss['loss'](pred,actual)

	def train(self,input_data,actual,n_chunks,verbose=True):
		# Randomly put training data into n_chunks
		# Assume "actual" is one-hot encoded if task="classification"
		shuffled_indices = np.random.permutation(input_data.shape[0])
		shuffled_input, shuffled_actual = input_data[shuffled_indices],actual[shuffled_indices]
		
		train_splits, actual_splits = np.split(shuffled_input,n_chunks), np.split(shuffled_actual,n_chunks)
		for batch_input,batch_labels in zip(train_splits,actual_splits):
			loss, accuracy_pct = self.optimizer(self,batch_input,batch_labels)

		if verbose:
			print(f"TRAIN loss, acc: {round(loss,4)}, {round(accuracy_pct,2)} %")

	def validate(self,test_input,test_labels):
		# Input test data and labels to get accuracy and loss
		predictions = self.predict(test_input)
		loss = self.get_loss(predictions,test_labels)

		accuracy_pct = 100*np.sum(predictions.argmax(axis=1)==test_labels.argmax(axis=1))/test_labels.shape[0]

		return loss, accuracy_pct





	