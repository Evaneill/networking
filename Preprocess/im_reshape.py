import numpy as np

def input_reshape(input_data,desired_shape_per_obs):
	# imagined for MNIST (N_obs x N_pixels) -> (N_obs x im_wid x im_len)

	return input_data.reshape((input_data.shape[0],desired_shape_per_obs[0],desired_shape_per_obs[1]))