import numpy as np

def jiggle_2D(im_array,n_pixels=1):
	# Take an (N_obs x im_w x im_len) image array and generate 4 new observations for each obs;
	# one new obs for each image moved up, down, left, right by n_pixels
	# space introduced is replaced with 0

	N_obs = im_array.shape[0]
	im_w, im_l = im_array.shape[1], im_array.shape[2]

	new_array = np.zeros(shape=(5*N_obs,im_array.shape[1],im_array.shape[2]))

	new_array[:N_obs,:,:]=im_array

	new_array[N_obs:2*N_obs,:-n_pixels,:-n_pixels]=im_array[:,n_pixels:,n_pixels:]
	new_array[2*N_obs:3*N_obs,n_pixels:,:-n_pixels]=im_array[:,:-n_pixels,n_pixels:]
	new_array[3*N_obs:4*N_obs,:-n_pixels,n_pixels:]=im_array[:,n_pixels:,:-n_pixels]
	new_array[4*N_obs:,n_pixels:,n_pixels:]=im_array[:,:-n_pixels,:-n_pixels]

	return new_array
