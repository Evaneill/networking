from .linear import linear
from .relu import relu
from .sigmoid import sigmoid

def get(act_name):
	if act_name=="linear":
		return linear

	elif act_name=="relu":
		return relu

	elif act_name=="sigmoid":
		return sigmoid