from .conv2d import Conv2D
from .dense import Dense

def get(layer_name):
	if layer_name=="Dense":
		return Dense

	elif layer_name=="Conv2D":
		return Conv2D