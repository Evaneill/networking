import numpy as np
from .. import Activations

class BaseLayer:

	def __init__(self,activation):

		self.activation = Activations.get(activation)
		self.learningrate = 0
		self.lmda = 0


class BaseConv(BaseLayer):
	
	def __init__(self,activation,filter_size,n_filter,skip=(1,1)):
		BaseLayer.__init__(self,activation)
		self.filters = [np.random.norm(scale=.1,size=(1,filter_size[0],filter_size[1])) for f in np.arange(n_filter)]
		self.filter_size = filter_size
		self.n_filter = n_filter