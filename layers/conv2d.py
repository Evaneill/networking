import numpy as np
from .base import BaseConv
from scipy import ndimage

class Conv2D(BaseConv):

	def _getshape(self,input_shape):
		''' Get the shape of output from this layer

		input (tuple): (N_obs)x(input len)x(input w) 
		output (tuple): Output shape that would fit into (N_obs)x(output shape)

		'''
		image_size = input_shape.shape[1:]
		if filter_size>image_size:
			raise Exception(f"Can't have filters ({filter_size}) be larger than image size ({image_size})")

		output_len = ((input_shape[0]-filter_size[0]+1)//skip[0])+1
		output_wid = ((input_shape[1]-filter_size[1]+1)//skip[1])+1

		return (output_len,output_wid)

	def activate(self,data_input):
		outputs=[]
		if isinstance(data_input,list):
			for im in data_input:

				outputs.append([self.activation['activ'](ndimage.convolve(data_input,filt)[:,1:-2:skip[0],1:-2:skip[1]]) for filt in self.filters])
		return outputs
