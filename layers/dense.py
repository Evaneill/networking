import numpy as np
from .base import BaseLayer
from .. import Losses
from ..Optimizers import gradient_descent as GD

class Dense(BaseLayer):
	
	def __init__(self,n_neurons,activation,biases=True,norm=0):

		BaseLayer.__init__(self,activation)

		if biases:
			self.biases = np.random.normal(scale=.1,size=(1,n_neurons))
		else:
			self.biases = False

		self.n_neurons = n_neurons

	def __str__(self):
		return f"Dense Layer with {self.n_neurons} neurons"

	def _getshape(self,input_shape=None):
		''' Get the shape of output from this layer
		input (np.array): (N_obs)x(Input shape) where input shape is a tuple
		output (np.array): Output shape that would fit into (N_obs)x(output shape)
		'''
		return (self.n_neurons,1)

	def activate(self,data_input):
		if isinstance(data_input,list):
				# Case that we have a list of filter outputs
				# list of (N_obs)x(filter out len)x(filter out w) -> one array of (N_obs)x(N_filter*filter out len)x(filter out w)
				data_input = np.hstack(data_input)

		N_obs = data_input.shape[0]

		data_input = data_input.reshape((N_obs,np.product(data_input.shape)//N_obs,1))

		if not hasattr(self,"weights"):
			self.weights = np.random.normal(scale=.1,size=(data_input.shape[1],self.n_neurons))

		# should return list containing single (N_obs)x(len)x(w) output array
		output=np.matmul(data_input.squeeze(),self.weights)
		if isinstance(self.biases,np.ndarray):
			output = output-np.repeat(self.biases,output.shape[0],axis=0)

		output = self.activation['activ'](output)
		self.latest_activation = output # Keep a record of latest activation

		return output

	def feed_back(self,dl):
		''' Take dl from feed_back on next layer, and produce dl for feed_back on previous'''
		dCdW=GD.dC(dl,self.latest_activation)

		if isinstance(self.biases,np.ndarray):
			self.biases = self.biases-self.learningrate*dl.mean(axis=0)

		print(self)
		print(f"dCdW shape: {dCdW.shape}")
		print(f"dl shape: {dl.shape}")
		print(f"layer weights shape: {self.weights.shape}")

		dl = np.matmul(dl,self.weights.transpose())*self.activation['deriv'](self.latest_activation)
		
		if norm:
			self.weights = (1-self.norm)*self.weights

		self.weights = self.weights - self.learningrate*dCdW

		return dl

	def _dC(self,error_l,layer_lminus1):
		# Given two lists of layer l and l-1 outputs for each image in the minibatch, what is the gradient matrix?
	    # for weight i,k it should be the average of activation i in layer l-1 times the error k in layer l
	    layer_lminus1 = layer_lminus1.reshape((layer_lminus1.shape[0],np.product(layer_lminus1.shape)//layer_lminus1.shape[0],1)).squeeze()
	    return np.matmul(layer_lminus1.transpose(),error_l)

	def get_gd_error(self,dl,prev_activation):
		prev_activation = prev_activation.reshape((prev_activation.shape[0],np.product(prev_activation.shape)//prev_activation.shape[0],1)).squeeze()
		return np.matmul(dl,self.weights.transpose())*self.activation['deriv'](prev_activation)

	def update(self,dCdW,dl):
		# Update the weights with feedback
		self.weights = self.weights - self.learningrate*dCdW

		if isinstance(self.biases,np.ndarray):
			self.biases = self.biases-self.learningrate*dl.mean(axis=0)
	