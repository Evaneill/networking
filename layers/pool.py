from .base import BaseConv

class Pool(BaseConv):
	# This pool layer will presume that the only type of previous input layer available is convolutional layer
	# This means input is necessarily a list of image stacks (N_obs)x(im_l)x(im_w)
	def __init__(self,filter_size,n_filter,pooling_function,skip=(1,1)):
		BaseConv.__init__(self,filter_size,n_filter,skip)
		self.pooling_function = pooling_function

	def activate(self,input):

	