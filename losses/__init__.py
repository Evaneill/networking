from .crossentropy import crossentropy
from .square import square
from .normalizers import *

def get(loss_name):
	if loss_name=="crossentropy":
		return crossentropy

	elif loss_name=="square":
		return square

