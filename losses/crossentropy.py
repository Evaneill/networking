import numpy as np

crossentropy =  {'loss': lambda prediction, actual: -1*np.sum((actual*np.log(prediction))+((1-actual)*np.log(1-prediction)))/actual.shape[0],# Calculated a single value
                        'deriv': lambda prediction, actual: (actual-prediction)/(prediction*(1-prediction))}