import numpy as np

def get(norm_name):
	if norm_name=="square_weights":
		return square_weights

def square_weights(network):
	
	normloss = 0
	for w in network.weights():
		normloss+=w.dot(w.transpose()).sum()

	return normloss