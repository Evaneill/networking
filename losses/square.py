import numpy as np
from . import normalizers

square =  {'loss': lambda prediction, actual: .5*np.sum((prediction-actual)**2)/prediction.shape[0],
            'deriv': lambda prediction, actual: prediction-actual}