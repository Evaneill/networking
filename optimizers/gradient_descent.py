import numpy as np

def GradientDescent(network,batch_input,batch_labels,verbose=True):
	# Batch labels should be one-hot encoded for classify task
	# i.e. Expects result of feed_forward() to be the same shape as batch_labels
	activations = network.feed_forward(batch_input)
	predictions = activations[-1]
	
	activ = network.layers[len(network.layers)-1].activation
	dl=network.loss['deriv'](predictions,batch_labels)*activ['deriv'](predictions)
	activations.reverse()
	previous_activations = activations[1:]
	
	for this_layer,previous_activation in zip(network.layers[::-1],previous_activations):

		dCdW=this_layer._dC(dl,previous_activation)

		this_layer.update(dCdW,dl)

		dl = this_layer.get_gd_error(dl,previous_activation)
		
		if this_layer.norm:
			this_layer.weights = (1-this_layer.norm)*this_layer.weights
		

	loss = network.get_loss(predictions,batch_labels)
	accuracy_pct = 100*np.sum(predictions.argmax(axis=1)==batch_labels.argmax(axis=1))/batch_labels.shape[0]

	return loss, accuracy_pct


def _dC(error_l,layer_lminus1):
    # Given two lists of layer l and l-1 outputs for each image in the minibatch, what is the gradient matrix?
    # for weight i,k it should be the average of activation i in layer l-1 times the error k in layer l
    return np.matmul(layer_lminus1.transpose(),error_l)